# About #
This script notifies the administrator about the high disk usage in each filesystem.

# Usage #
```
#!bash
./threshold_alert_v2.ksh
```
