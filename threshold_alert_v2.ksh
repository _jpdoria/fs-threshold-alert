#!/usr/bin/ksh
# Script:       Threshold Email Alert v2.0 (threshold_alert_v2.ksh) 
# Function:     This script notifies the administrator about the high disk usage in each filesystem.
# Author:       John Paul P. Doria
# Date Created: July 16, 2013

logdate=`date +%m%d%y%H%M%S`
tmplog1=$HOME/.1$$${logdate}.tmp
tmplog2=$HOME/.2${logdate}$$.tmp
newmail=$HOME/.newmail_${$}${logdate}.tmp
fsreport=$HOME/fs_util_report_${logdate}.csv

export PATH=$PATH:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:

trap "echo \"Interrupted.\"; rm -f $tmplog1 $tmplog2 $newmail $fsreport > /dev/null 2>&1" 1 2 3 15

getList() {
	df -h | awk {'print $6'} | grep -v Mounted
}

threshold() {
	df -h $fs | awk {'getline;print $5'} | sed 's/%//g'
}

cleanup() {
	rm -f $tmplog1 $tmplog2 $newmail $fsreport
}

newtmp() {
	echo "FS Utilization Report" >> $fsreport
	echo "Report Date: `date`\n" >> $fsreport
	echo "Filesystem(s)" >> $tmplog1
	echo "Utilization" >> $tmplog2
	echo "This is an automated email. Please do not reply." >> $newmail
	echo "Attached herewith is the FS Utilization Report generated as of `date`." >> $newmail
}

sendmail() {
	echo ">>> Sending report... \c"
	paste -d, $tmplog1 $tmplog2 >> $fsreport
	(cat $newmail; uuencode $fsreport fs_util_report_${logdate}.csv) | mailx -s "FS Utilization Report as of `date`" -r "Admins" "sysadmins@email.com"
	sleep 3
	echo "Sent!"
	echo "===================================================================="
	echo " Total no. of FS checked\t= `getList | wc -l | sed 's/^[ \t]*//g'`"
	echo " End Time\t\t\t= `date`"
	echo "===================================================================="
}

scan() {
	echo "===================================================================="
	echo " Script\t\t= threshold_alert_v2.ksh"
	echo " OS\t\t= `uname -srv`"
	echo " Start Time\t= `date`"
	echo "====================================================================\n"
	echo "********************************************************************"
	echo " Getting the list..."
	echo "********************************************************************"
	for fs in `getList`; do
		echo ">>> Checking $fs... \c"
		if [ `threshold` -ge 80 ]; then
			echo "$fs" >> $tmplog1
			echo "`threshold`%" >> $tmplog2
			echo "\033[1m\033[5m`threshold`%!!!\033[0m\033[0m"
		else
			echo "Below 80%. Skipping..."
		fi
	done
}

main() {
	newtmp && scan && sendmail && cleanup
	exit 0
}

main
